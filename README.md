# README #

Einfaches Beispiel zur SAML2 kommunikation mit dem AD FS 3.

### Ablauf ###

Nach dem erfolgreichen Deployment steht am Applikationsserver die Anwendung unter /opensaml zur Verfügung.

* Durch den Aufruf der url opensaml/app/appservlet wird eine Servletfilter aktiv
Beispiel: https://wildfly.emaxtest.local:8443/opensaml/app/appservlet

* Konfiguration des Servletfilter

Dieser Filter sieht nach ob der Benutzer bereits authentifiziert ist. Ist der Benutzer noch nicht authentifiziert leitet er den Browserrequest an den Login vom AD FS weiter. Der AccessFilter baut dazu einen AuthnRequest und sendet diesen an das AF FS.

Der Request wird im Log mit ausgegeben:
~~~~
<?xml version="1.0" encoding="UTF-8"?>
<saml2p:AuthnRequest xmlns:saml2p="urn:oasis:names:tc:SAML:2.0:protocol" Destination="https://fs.emaxtest.local/adfs/ls/idpinitiatedSignon" ID="_5fed1e65e20620a2fdfeea2253e9c9fc" IssueInstant="2016-01-19T18:37:32.842Z" ProtocolBinding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Version="2.0">
	<saml2:Issuer xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion">https://wildfly.emaxtest.local:8443/opensaml</saml2:Issuer>
	<saml2p:RequestedAuthnContext Comparison="minimum">
		<saml2:AuthnContextClassRef xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml2:AuthnContextClassRef>
	</saml2p:RequestedAuthnContext>
</saml2p:AuthnRequest>
~~~~

* Nach der erfolgreichen Anmeldung am AD FS leitet dieses an den Endpunkt weiter, welches im AD FS konfiguriert wurde
Beispiel: https://wildfly.emaxtest.local:8443/opensaml//sp/consumer

* Das ConsumerServlet nimmt den SAMLResponse entgegen und liest die darin enthaltenen Claims aus.
~~~~
<?xml version="1.0" encoding="UTF-8"?>
<samlp:Response xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" Consent="urn:oasis:names:tc:SAML:2.0:consent:unspecified" Destination="https://wildfly.emaxtest.local:8443/opensaml/sp/consumer" ID="_6e96d29a-e8cb-405f-90be-8bea4ceaf962" InResponseTo="_5a28292a099aa49fc9c317ef5ecc3e91" IssueInstant="2016-01-20T16:27:18.583Z" Version="2.0">
	<Issuer xmlns="urn:oasis:names:tc:SAML:2.0:assertion">http://fs.emaxtest.local/adfs/services/trust</Issuer>
	<samlp:Status>
		<samlp:StatusCode Value="urn:oasis:names:tc:SAML:2.0:status:Success"/>
	</samlp:Status>
	<Assertion xmlns="urn:oasis:names:tc:SAML:2.0:assertion" ID="_778116f6-7b96-48f1-af4e-e8c8b77a5ccb" IssueInstant="2016-01-20T16:27:18.582Z" Version="2.0">
		<Issuer>http://fs.emaxtest.local/adfs/services/trust</Issuer>
		<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
			<ds:SignedInfo>
				<ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
				<ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
				<ds:Reference URI="#_778116f6-7b96-48f1-af4e-e8c8b77a5ccb">
					<ds:Transforms>
						<ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
						<ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
					</ds:Transforms>
					<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
					<ds:DigestValue>11CStl55ca/3zQIv/XeoGcvb4MpIBRB3zHglfvMKTgo=</ds:DigestValue>
				</ds:Reference>
			</ds:SignedInfo>
			<ds:SignatureValue>P7ORO9ZcT6meeCo7cnu5h1oZpoNcvgHKBwCZMX4BhZNHhBu4PADpMJOQU/5I/ZkJCwZNCPlmTcUevX2klSgdUa0VLJpXufQBr0OLENHAMSgH+DvxgnoMGHrkOF3cn9Tee+9qr9dcY1f/OdwejXgBz1bmm4/sZWJh6+lQE0WLTTasOKhUslbZJS4MISIRsJ7lFU5eI2Uv5vZP6yVYud0hCAhR+Syyn2+fo/h5+T1umuqneH+ya0xNEY4vBFNX++HvFkelA0Ggx0YbXLASg6u59JdS8U2SiSlKhaX+f6QCRnTPMJPkO5OFJGGamgm4XvFJTuRgUPiX+yn8g+8yxlGkCQ==</ds:SignatureValue>
			<KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
				<ds:X509Data>
					<ds:X509Certificate>MIIC3jCCAcagAwIBAgIQGI0vyGJ2QZRHpSrSwz1CrDANBgkqhkiG9w0BAQsFADArMSkwJwYDVQQDEyBBREZTIFNpZ25pbmcgLSBmcy5lbWF4dGVzdC5sb2NhbDAeFw0xNTA2MTUxNTE0NTJaFw0xNjA2MTQxNTE0NTJaMCsxKTAnBgNVBAMTIEFERlMgU2lnbmluZyAtIGZzLmVtYXh0ZXN0LmxvY2FsMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjYuIVfZGy0FJol6MdnhUwsMHjr0TO14xxNEg6i/UAMaglAWD+FSYF9HR3UIBaVwDNrfc3lsuUBlklel2l4xAx6fiEHxA9iw9C/HRY3NqbJqv9oSYp7a6FmqElr8MO+i7Zg9wxtk870EnFogZsYiJyMjghePLCbzAbNDYRejcv+xkHyWkVX0d147maT6wFJaIp03j0kylpMmi1Usjb/whh3HT9xGRb6KPGyRNpNhy17Q/rnks3cgp7jy0vp0m7hnpVryXQoJiVOQVmJjmnzQMwdKtuDtJmtfeaRVLSE90BazOu9NTWku/vihJDuHI0iuN/2FIclDAg6HQLwSEmRcrsQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQA21o5Y1sRfPOMFTO8XsMNIzrSQgyL0kJFGzLzND3r2UcxkcurePD+I7i7J2W6ZWtxv5ufbQYBzMDaRj1TOFYvnqa5ekOBoN8Dn6oLijvP+V/drxcEN/16dDLW/ZgWPS2f1YZ432xiDk3jP8Eii9EapP4ULEzSdKbEacHqphFhvbVq8qHO7fz3eaHHqzA3MviVPruiKozqRnBi//W6Y3h/lgXSaZeIf4mYaqkdt8p7izR01oTvptyJUao0kVKGpbE5bas6k+AhnS0RBIclvdim3gI8A/xPTIQ7rp+ATJCXLfZ0NxtGR9vACSO8O6WBlqWeU/sPxnDqrwwtxTilJ1GyR</ds:X509Certificate>
				</ds:X509Data>
			</KeyInfo>
		</ds:Signature>
		<Subject>
			<NameID>user1</NameID>
			<SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
				<SubjectConfirmationData InResponseTo="_5a28292a099aa49fc9c317ef5ecc3e91" NotOnOrAfter="2016-01-20T16:32:18.583Z" Recipient="https://wildfly.emaxtest.local:8443/opensaml/sp/consumer"/>
			</SubjectConfirmation>
		</Subject>
		<Conditions NotBefore="2016-01-20T16:27:18.574Z" NotOnOrAfter="2016-01-20T17:27:18.574Z">
			<AudienceRestriction>
				<Audience>urn:sso-test:opensaml:com</Audience>
			</AudienceRestriction>
		</Conditions>
		<AttributeStatement>
			<Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname">
				<AttributeValue>user1</AttributeValue>
			</Attribute>
			<Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress">
				<AttributeValue>ben@utzer.at</AttributeValue>
			</Attribute>
			<Attribute Name="http://schemas.xmlsoap.org/claims/Group">
				<AttributeValue>emaxtest.local\Domänen-Benutzer</AttributeValue>
				<AttributeValue>emaxtest.local\TD_GRP_SYSTEM_READ</AttributeValue>
			</Attribute>
		</AttributeStatement>
		<AuthnStatement AuthnInstant="2016-01-20T16:27:18.281Z" SessionIndex="_778116f6-7b96-48f1-af4e-e8c8b77a5ccb">
			<AuthnContext>
				<AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</AuthnContextClassRef>
			</AuthnContext>
		</AuthnStatement>
	</Assertion>
</samlp:Response>
~~~~

* In diesem Fall ist der Endpunkt im AD FS nicht verschlüsselt und somit werden die Daten nur encoded (Base64) übertragen.

### Konfiguration ###

* Servlet Filter in der web.xml
~~~~
    <filter>
        <filter-name>AccessFilter</filter-name>
        <filter-class>at.emax.opensaml.sp.AccessFilter</filter-class>
    </filter>

    <filter-mapping>
        <filter-name>AccessFilter</filter-name>
        <url-pattern>/app/*</url-pattern>
        <dispatcher>REQUEST</dispatcher>
    </filter-mapping>
~~~~

* Consumer in der web.xml
~~~~
    <servlet>
        <servlet-name>ConsumerServlet</servlet-name>
        <servlet-class>at.emax.opensaml.sp.ConsumerServlet</servlet-class>
    </servlet>

    <servlet-mapping>
        <servlet-name>ConsumerServlet</servlet-name>
        <url-pattern>/sp/consumer</url-pattern>
    </servlet-mapping>
~~~~
* IPD Url
**at.emax.opensaml.idp.IDPConstants**
~~~~
public static final String SSO_SERVICE = "https://fs.emaxtest.local/adfs/ls/idpinitiatedSignon";
~~~~
* SP Seitenbezeichnung im AD FS
**at.emax.opensaml.sp.SPConstants**
~~~~
public static final String SP_ENTITY_ID = "urn:sso-test:opensaml:com";
~~~~