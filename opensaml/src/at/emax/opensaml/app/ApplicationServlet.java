package at.emax.opensaml.app;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet acts as the resource that the access filter is protecting
 */
public class ApplicationServlet extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.getWriter().append("<h1>Willkommen</h1>");
        resp.getWriter().append("Das ist eine geschützte Resource. Du bist authentifiziert.");
    }
}
