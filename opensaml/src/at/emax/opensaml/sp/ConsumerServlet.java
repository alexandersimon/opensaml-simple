package at.emax.opensaml.sp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.binary.Base64;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Response;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import at.emax.opensaml.OpenSAMLUtils;

public class ConsumerServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(ConsumerServlet.class);

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doRequest(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.info("Artifact received");

		try {

			Response artifactResponse = buildResponseFromRequest(req);
			logger.info("ArtifactResponse received");
			logger.info("ArtifactResponse: ");
			OpenSAMLUtils.logSAMLObject(artifactResponse);

			List<AttributeStatement> attributestatements = artifactResponse.getAssertions().get(0)
					.getAttributeStatements();

			for (AttributeStatement attributeStatement : attributestatements) {
				List<Attribute> attributes = attributeStatement.getAttributes();
				for (Attribute attribute : attributes) {
					logger.info("Attribute: " + attribute.getName());
					List<XMLObject> values = attribute.getAttributeValues();
					for (XMLObject value : values) {
						logger.info("Value: " + value.getDOM().getTextContent());
					}
				}
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (UnmarshallingException e) {
			e.printStackTrace();
		} finally {

		}

		setAuthenticatedSession(req);
		redirectToGotoURL(req, resp);
	}

	private void setAuthenticatedSession(HttpServletRequest req) {
		req.getSession().setAttribute(SPConstants.AUTHENTICATED_SESSION_ATTRIBUTE, true);
	}

	private void redirectToGotoURL(HttpServletRequest req, HttpServletResponse resp) {
		String gotoURL = (String) req.getSession().getAttribute(SPConstants.GOTO_URL_SESSION_ATTRIBUTE);
		logger.info("Redirecting to requested URL: " + gotoURL);
		try {
			resp.sendRedirect(gotoURL);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Response buildResponseFromRequest(final HttpServletRequest req)
			throws ParserConfigurationException, SAXException, IOException, UnmarshallingException {
		String responseString = req.getParameter("SAMLResponse");
		String responseXml = new String(Base64.decodeBase64(responseString));

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();
		ByteArrayInputStream is = new ByteArrayInputStream(responseXml.getBytes());
		Document document = docBuilder.parse(is);
		Element element = document.getDocumentElement();

		UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();

		Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(element);

		XMLObject xmlObj = unmarshaller.unmarshall(element);
		Response response = (Response) xmlObj;

		return response;
	}

}
