package at.emax.opensaml.sp;


public class SPConstants {
    public static final String SP_ENTITY_ID = "urn:sso-test:opensaml:com";
    public static final String AUTHENTICATED_SESSION_ATTRIBUTE = "authenticated";
    public static final String GOTO_URL_SESSION_ATTRIBUTE = "gotoURL";
}
